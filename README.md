# Platzi Badges

Plataforma para llevar registro de los asistentes a la [Platzi](https://platzi.com/) Conf, haciendo posible que cada usuario registrado pueda ingresar su información personal. Cada uno de ellos tendrá un rol diferente y esto se verá reflejado en su badge. Por último, crearás una página que permitirá listar a todos los participantes y filtrarlos por tipo de rol, o hacer búsqueda por nombre.

Proyecto realizado en React

Link demo: https://cmyaraplatzibadges.netlify.app


| Nombre  |  Email |
| ------------ | ------------ |
|  Cristhian Yara |  cmyarap@gmail.com |


### Deployment
1. Clone project (it assume you have previously installed git)<br />
    `git clone `
2. Go to project<br />
    `cd platzi-badges`
3. Update Packages<br />
    `npm install`
4. Start project<br />
    `npm start`

### Instructions (done to create it)
1. Install React CLI (it assume you have previously installed [node](https://nodejs.org/))<br />
    `npm install -g create-react-app`
2. Create project<br />
    `create-react-app platzi-badges`
3. Go to project<br />
    `cd platzi-badges`
4. Start project<br />
    `npm start`

### Package Installed
- React-Router<br />
    `npm install react-router-dom`
- md5<br />
    `npm install md5`
