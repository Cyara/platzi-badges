import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import './styles/Badges.css';
import confLogo from '../images/platziconf-logo.svg';
import BadgesList from '../components/BadgesList';
import PageLoading from '../components/PageLoading';
import MiniLoader from '../components/MiniLoader';
import PageError from '../components/PageError';

import api from '../api';

class Badges extends Component{

    // constructor(props){
    //     super(props);
    //     console.log('1. constructor()')
    //     this.state = {
    //         data: [],
    //     };
    // }

    // componentDidMount(){
    //     console.log('3. componentDidMount()')

    //     this.timeoutId = setTimeout(() => {
    //         this.setState({
    //             data:[
    //                 {
    //                     id: '2de30c42-9deb-40fc-a41f-05e62b5939a7',
    //                     firstName: 'Freda',
    //                     lastName: 'Grady',
    //                     email: 'Leann_Berge@gmail.com',
    //                     jobTitle: 'Legacy Brand Director',
    //                     twitter: 'FredaGrady22221-7573',
    //                     avatarUrl:
    //                     'https://www.gravatar.com/avatar/f63a9c45aca0e7e7de0782a6b1dff40b?d=identicon',
    //                 },
    //                 {
    //                     id: 'd00d3614-101a-44ca-b6c2-0be075aeed3d',
    //                     firstName: 'Major',
    //                     lastName: 'Rodriguez',
    //                     email: 'Ilene66@hotmail.com',
    //                     jobTitle: 'Human Research Architect',
    //                     twitter: 'MajorRodriguez61545',
    //                     avatarUrl:
    //                     'https://www.gravatar.com/avatar/d57a8be8cb9219609905da25d5f3e50a?d=identicon',
    //                 },
    //                 {
    //                     id: '63c03386-33a2-4512-9ac1-354ad7bec5e9',
    //                     firstName: 'Daphney',
    //                     lastName: 'Torphy',
    //                     email: 'Ron61@hotmail.com',
    //                     jobTitle: 'National Markets Officer',
    //                     twitter: 'DaphneyTorphy96105',
    //                     avatarUrl:
    //                     'https://www.gravatar.com/avatar/e74e87d40e55b9ff9791c78892e55cb7?d=identicon',
    //                 },
    //             ]
    //         })
    //     }, 3000);
    // }

    // componentDidUpdate(prevProps, prevState){
    //     console.log('5. componentDidUpdate')
    //     console.log({prevProps:prevProps,prevState:prevState})
    //     console.log({props: this.props, state:this.state})
    // }

    // componentWillUnmount(){
    //     console.log('6. componentWillUnmount')
    //     clearTimeout(this.timeoutId)
    //     console.log('timeOutId: '+this.timeoutId)
    // }

    state = {
        loading: true,
        error:null,
        data: undefined,
    }

    componentDidMount(){
        this.fetchData()

        this.intervalId = setInterval(() => {
            this.fetchData()
        }, 5000);
    }

    componentWillUnmount(){
        clearInterval(this.intervalId)
    }

    fetchData = async () => {
        this.setState({ loading: true, error: null });
        try {
            const data = await api.badges.list();
            this.setState({ loading: false, data: data });
        } catch (error) {
            this.setState({ loading: false, error: error });
        }
    };

    render(){
        // console.log('2. render()')

        if (this.state.loading === true && !this.state.data)
            return <PageLoading />; // Opción react-loading-skeleton

        if (this.state.error)
            return <PageError error={this.state.error}/>//`Error: ${this.state.error.message}`;

        return(
            <React.Fragment>
                <div className="Badges">
                    <div className="Badges__hero">
                        <div className="Badges__container">
                            <img className="Badges_conf-logo" src={confLogo} alt="Conf Logo"/>
                        </div>
                    </div>
                </div>

                <div className="Badges__container">
                    <div className="Badges__buttons">
                        <Link to="/badge/new" className="btn btn-primary">
                            New Badge
                        </Link>
                    </div>
                    <BadgesList badges={this.state.data}/>

                    {this.state.loading && <MiniLoader/>}
                </div>
            </React.Fragment>
        )
    }
}

export default Badges;



// Constructor(props){
// 	/*Este método se ejecuta cuando se instancia un componente. Nos permite definir el
//  estado inicial del 		componente, hacer bind de métodos y definir propiedades internas en
// las que podemos guardar muchos datos diferente*/
// }

// componentWillMonunt(){
// 	/*Este método se ejecuta cuando el componente se está por renderizar.
//  En este punto es posible modificar el estado del componente sin causar una actualización
//  (y por lo tanto no renderizar dos veces el componente).*/
// }

// render(){
// 	/*En este momento de la fase de montado se van a tomar las propiedades, el estado y el contexto y se va a generar la UI inicial de este componente*/
// }

// componentDidMount(){
// 	/*Este último método de la fase de montado se ejecuta una vez el componente se renderizó en el navegador y nos permite interactuar con el DOM o las otras APIs del navegador (geolocation, navigator, notificaciones, etc.).*/
// }

// componentWillReceiveProps(nextProps){
// 	/*Este método se ejecuta inmediatamente después que el componente reciba nuevas propiedades. En este punto es posible actualizar el estado para que refleje el cambio de propiedades, ya sea reiniciando su valor inicial o cambiándolo por uno nuevo.*/
// }

// shouldComponentUpdate(nextProps, nextState){
// 	/*Este método (el cual debe ser puro) se ejecuta antes de empezar a actualizar un componente, cuando llegan las nuevas propiedades (nextProps) y el nuevo estado (nextState).

// Acá es posible validar que estos datos sean diferentes de los anteriores (this.props y this.state) y devolver true o false dependiendo de si queremos volver a renderizar o no el componente.*/
// }

// componentWillUpdate(nextProps, nextState){
// 	/*Una vez el método anterior devolvió true se ejecuta este método, acá es posible realizar cualquier tipo de preparación antes de que se actualice de la UI*/
// }

// https://platzi.com/blog/ciclo-de-vida-de-un-componente-de-reactjs/