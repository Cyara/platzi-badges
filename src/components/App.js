import React from 'react';
import {BrowserRouter as Router, Switch,  Route} from 'react-router-dom';
// import { Redirect, Route } from "react-router-dom";

import BadgeNew from '../pages/BadgeNew';
import BadgeEdit from '../pages/BadgeEdit';
import BadgeDetailsContainer from '../pages/BadgeDetailsContainer';
import Badges from '../pages/Badges';
import Home from '../pages/Home';
import NotFound from '../components/NotFound';
import Layout from './Layout';

function App(){
    return (
        <Router>
            <Layout>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/badges" component={Badges}/>
                    <Route exact path="/badge/new" component={BadgeNew}/>
                    <Route exact path="/badge/:badgesId/edit" component={BadgeEdit}/>
                    <Route exact path="/badge/:badgesId" component={BadgeDetailsContainer}/>
                    <Route component={NotFound}/>
                    {/* Otra forma de redireccionar a la página de error
                        <Route path="/404" component={NotFound} />
                        <Redirect from="*" to="/404" />
                    */}
                </Switch>
            </Layout>
        </Router>
    )
}

export default App;